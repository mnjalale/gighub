﻿using GigHub.Core;
using GigHub.Core.Repositories;
using GigHub.Persistence;
using GigHub.Persistence.Repositories;
using StructureMap;

namespace GigHub.IoC
{
    public class GigHubRegistry : Registry
    {
        public GigHubRegistry()
        {
            For<ApplicationDbContext>().Use<ApplicationDbContext>();

            For<IUnitOfWork>().Use<UnitOfWork>();

            For<IAttendanceRepository>().Use<AttendanceRepository>();
            For<IFollowingRepository>().Use<FollowingRepository>();
            For<IGenreRepository>().Use<GenreRepository>();
            For<IGigRepository>().Use<GigRepository>();
            For<INotificationRepository>().Use<NotificationRepository>();
            For<IUserNotificationRepository>().Use<UserNotificationRepository>();

        }
    }
}