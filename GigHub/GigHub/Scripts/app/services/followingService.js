﻿var FollowingService = function () {

    var followArtist = function (followeeId,done,fail) {
        $.post("/api/following/follow", { followeeId: followeeId })
            .done(done)
            .fail(fail);
    };

    var unfollowArtist = function (followeeId,done,fail) {
        $.post("/api/following/unfollow", { followeeId: followeeId })
               .done(done)
               .fail(fail);
    };

    return {
        followArtist: followArtist,
        unfollowArtist: unfollowArtist
    }
}();