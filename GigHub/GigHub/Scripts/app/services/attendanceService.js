﻿var AttendanceService = function () {

    var createAttendance = function (gigId, done, fail) {
        $.post("/api/attendances/attend", { gigId: gigId })
               .done(done)
               .fail(fail);
    }

    var deleteAttendance = function (gigId, done, fail) {
        $.post("/api/attendances/cancel", { gigId: gigId })
            .done(done)
            .fail(fail);
    }

    return {
        createAttendance: createAttendance,
        deleteAttendance: deleteAttendance
    }
}();