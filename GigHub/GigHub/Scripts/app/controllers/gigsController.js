﻿var GigsController = function (attendanceService) {

    var button;

    var init = function (container) {

        $(container).on("click", ".js-toggle-attendance", toggleAttendance);

        // Issues with the implemenation below:
        // 1. Works only for documents that are currently in the DOM. If we have "load more.." the toggleattendance method will not be called for objects added later
        // 2. You will end up with a separate handler for all elements matching this criteria ".js-toggle-attendance". Bad memory management
        // $(".js-toggle-attendance").click(toggleAttendance);
    };

    var toggleAttendance = function (e) {

        button = $(e.target);
        var gigId = button.attr("data-gig-id");

        var currentStatus = button.text().trim();

        if (currentStatus === "Going") {
            attendanceService.deleteAttendance(gigId, done, fail);
        }
        else if (currentStatus === "Going?") {
            attendanceService.createAttendance(gigId, done, fail);
        }
    };

    var done = function () {
        var text = button.text().trim() === "Going?" ? "Going" : "Going?";
        button.toggleClass("btn-info").toggleClass("btn-default").text(text);
    };

    var fail = function () {
        alert("Something went wrong");
        //alert(error.responseJSON.Message || "Something went wrong");
    };

    return {
        init: init
    }

}(AttendanceService);