﻿var GigDetailsController = function (followingService) {

    var button;

    var init = function (container) {
        $(container).on("click", ".js-toggle-following", toggleFollow);
    };

    var toggleFollow = function (e) {

        button = $(e.target);
        var followeeId = button.attr("data-followee-id");
        if (button.text().trim() === "Follow") {
            followingService.followArtist(followeeId,done,fail);
        } else if (button.text().trim() === "Following") {
            followingService.unfollowArtist(followeeId, done, fail);
        }

    };

    var done = function () {
        var text = button.text().trim() === "Follow" ? "Following" : "Follow";
        button.toggleClass("btn-info").toggleClass("btn-default").text(text);
    };

    var fail = function () {
        alert("Something went wrong");
    };

    return {
        init: init
    };
}(FollowingService);