﻿using GigHub.Core;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace GigHub.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly IUnitOfWork unitOfWork;

        public BaseController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        protected string UserId
        {
            get
            {
                var userId = User.Identity.GetUserId();
                return userId;
            }
        }
    }
}