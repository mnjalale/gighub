﻿using GigHub.Core;
using GigHub.Core.Models;
using GigHub.Core.ViewModels.Gigs;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Controllers
{
    public class GigsController : BaseController
    {
        public GigsController(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        [Authorize]
        public ActionResult Create()
        {
            var viewModel = new GigFormViewModel()
            {
                Heading = "Add a gig",
                Genres = unitOfWork.GenreRepository.Get()
            };
            return View("GigForm", viewModel);
        }

        [Authorize]
        public ActionResult Edit(int id)
        {
            var gig = unitOfWork.GigRepository.GetGig(id);

            if (gig == null)
            {
                return HttpNotFound();
            }

            if (gig.ArtistId != UserId)
            {
                return new HttpUnauthorizedResult();
            }

            var viewModel = new GigFormViewModel()
            {
                Id = gig.Id,
                Heading = "Edit a Gig",
                Date = gig.DateTime.ToString("d MMM yyyy"),
                Time = gig.DateTime.ToString("HH:mm"),
                GenreId = gig.GenreId,
                Venue = gig.Venue,
                Genres = unitOfWork.GenreRepository.Get()
            };

            return View("GigForm", viewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GigFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Genres = unitOfWork.GenreRepository.Get();
                return View("GigForm", viewModel);
            }

            var gig = new Gig()
            {
                ArtistId = User.Identity.GetUserId(),
                DateTime = viewModel.GetDateTime(),
                GenreId = viewModel.GenreId,
                Venue = viewModel.Venue
            };

            unitOfWork.GigRepository.Add(gig);
            unitOfWork.Save();

            return RedirectToAction("Mine", "Gigs");
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(GigFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Genres = unitOfWork.GenreRepository.Get();
                return View("GigForm", viewModel);
            }

            var gig = unitOfWork.GigRepository.GetGigWithAttendees(viewModel.Id);

            if (gig == null)
            {
                return HttpNotFound();
            }

            if (gig.ArtistId != UserId)
            {
                return new HttpUnauthorizedResult();
            }

            unitOfWork.GigRepository.Update(gig, viewModel.GetDateTime(), viewModel.GenreId, viewModel.Venue);
            unitOfWork.Save();

            return RedirectToAction("Mine", "Gigs");
        }

        [Authorize]
        public ActionResult Attending()
        {
            var gigsAttending = unitOfWork.GigRepository.GetGigsUserAttending(UserId);
            var attendances = unitOfWork.AttendanceRepository.GetFutureAttendances(UserId).ToLookup(a => a.GigId);

            var gigsViewModel = new GigsViewModel()
            {
                UpcomingGigs = gigsAttending,
                UserAttendances = attendances,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Gigs I'm Attending"
            };

            return View("Gigs", gigsViewModel);
        }

        [Authorize]
        public ActionResult Mine()
        {
            var gigs = unitOfWork.GigRepository.GetUpcomingGigsByArtist(UserId);

            return View(gigs);

        }

        [HttpPost]
        public ActionResult Search(GigsViewModel viewModel)
        {
            return RedirectToAction("Index", "Home", new { query = viewModel.SearchTerm });
        }

        public ActionResult Details(int id)
        {
            var gig = unitOfWork.GigRepository.GetGig(id);

            if (gig == null)
            {
                return HttpNotFound();
            }

            var viewModel = new GigDetailsViewModel()
            {
                Gig = gig
            };

            if (User.Identity.IsAuthenticated)
            {
                viewModel.IsFollowing = unitOfWork.FollowingRepository.GetFollowing(gig.ArtistId, UserId) != null;
                viewModel.IsAttending = unitOfWork.AttendanceRepository.GetAttendance(gig.Id, UserId) != null;
            }

            return View("GigDetails", viewModel);

        }
    }
}