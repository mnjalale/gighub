﻿using GigHub.Core;
using GigHub.Core.Dtos;
using System.Web.Http;

namespace GigHub.Controllers.Api
{
    [Authorize]
    [Route("api/gigs")]
    public class GigsController : BaseApiController
    {
        public GigsController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var gigs = unitOfWork.GigRepository.GetUpcomingGigs();
            return Ok(gigs);
        }

        [HttpPost]
        public IHttpActionResult Cancel(CancelDto dto)
        {
            var gig = unitOfWork.GigRepository.GetGigWithAttendees(dto.GigId);

            if (gig == null || gig.IsCancelled)
            {
                return NotFound();
            }

            if (gig.ArtistId != UserId)
            {
                return Unauthorized();
            }

            gig.Cancel();

            unitOfWork.Save();

            return Ok();
        }

    }
}