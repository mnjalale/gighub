﻿using GigHub.Core;
using GigHub.Core.Models.Extensions;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using WebGrease.Css.Extensions;

namespace GigHub.Controllers.Api
{
    [Authorize]
    [Route("api/notifications")]
    public class NotificationsController : BaseApiController
    {
        public NotificationsController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public IHttpActionResult GetNewNotifications()
        {
            var notifications = unitOfWork.NotificationRepository.GetUnreadNotifications(UserId)
                                .OrderByDescending(c => c.DateTime)
                                .ToList();

            var dtos = notifications.Select(c => c.ToDto());

            return Ok(dtos);
        }

        [HttpPost]
        public IHttpActionResult MarkAsRead()
        {
            var unreadNotifications = unitOfWork.UserNotificationRepository.GetUnreadUserNotifications(UserId);

            unreadNotifications.ForEach(c => c.Read());

            unitOfWork.Save();

            return Ok();
        }
    }
}
