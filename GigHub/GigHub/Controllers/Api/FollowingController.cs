﻿using GigHub.Core;
using GigHub.Core.Dtos;
using GigHub.Core.Models;
using System.Web.Http;

namespace GigHub.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/following")]
    public class FollowingController : BaseApiController
    {
        public FollowingController(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        [HttpPost]
        [Route("follow")]
        public IHttpActionResult FollowArtist(FollowingDto dto)
        {
            var exists = unitOfWork.FollowingRepository.GetFollowing(dto.FolloweeId, UserId) != null;
            if (exists)
            {
                return BadRequest("You are already following this artist.");
            }

            var following = new Following()
            {
                FolloweeId = dto.FolloweeId,
                FollowerId = UserId
            };

            unitOfWork.FollowingRepository.Add(following);

            unitOfWork.Save();

            return Ok();
        }

        [HttpPost]
        [Route("unfollow")]
        public IHttpActionResult UnFollowArtist(FollowingDto dto)
        {
            var following = unitOfWork.FollowingRepository.GetFollowing(dto.FolloweeId, UserId);
            if (following == null)
            {
                return NotFound();
            }

            unitOfWork.FollowingRepository.Delete(following);
            unitOfWork.Save();

            return Ok();
        }

    }
}