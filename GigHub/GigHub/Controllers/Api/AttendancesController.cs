﻿using GigHub.Core;
using GigHub.Core.Dtos;
using GigHub.Core.Models;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace GigHub.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/attendances")]
    public class AttendancesController : BaseApiController
    {
        public AttendancesController(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        [HttpPost]
        [Route("attend")]
        public IHttpActionResult Attend(AttendanceDto dto)
        {
            var attendeeId = User.Identity.GetUserId();

            var attendance = unitOfWork.AttendanceRepository.GetAttendance(dto.GigId, attendeeId);
            if (attendance != null)
            {
                return BadRequest("The attendance already exists.");
            }

            attendance = new Attendance()
            {
                GigId = dto.GigId,
                AttendeeId = attendeeId
            };

            unitOfWork.AttendanceRepository.Add(attendance);
            unitOfWork.Save();

            return Ok();
        }

        [HttpPost]
        [Route("cancel")]
        public IHttpActionResult Cancel(AttendanceDto dto)
        {
            var attendeeId = User.Identity.GetUserId();

            var attendance = unitOfWork.AttendanceRepository.GetAttendance(dto.GigId, attendeeId);
            if (attendance == null)
            {
                return NotFound();
            }

            unitOfWork.AttendanceRepository.Delete(attendance);
            unitOfWork.Save();
            return Ok();
        }

        [HttpGet]
        [Route("get/{gigId}")]
        public IHttpActionResult IsAttendingGig(int gigId)
        {
            var attending = unitOfWork.AttendanceRepository.GetAttendance(gigId, UserId) != null;
            return Ok(attending);

        }
    }
}
