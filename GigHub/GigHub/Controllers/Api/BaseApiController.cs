﻿using GigHub.Core;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace GigHub.Controllers.Api
{
    public abstract class BaseApiController : ApiController
    {
        protected IUnitOfWork unitOfWork;

        public BaseApiController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public string UserId
        {
            get
            {
                var userId = User.Identity.GetUserId();
                return userId;
            }
        }
    }
}