﻿using GigHub.Core;
using GigHub.Core.ViewModels.Gigs;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public ActionResult Index(string query = null)
        {
            var upcomingGigs = unitOfWork.GigRepository.GetUpcomingGigs();

            if (!string.IsNullOrWhiteSpace(query))
            {
                upcomingGigs = upcomingGigs.Where(c => c.Artist.Name.Contains(query)
                                                    || c.Genre.Name.Contains(query)
                                                    || c.Venue.Contains(query));
            }

            var attendances = unitOfWork.AttendanceRepository.GetFutureAttendances(UserId).ToLookup(a => a.GigId);

            var viewModel = new GigsViewModel()
            {
                UpcomingGigs = upcomingGigs,
                UserAttendances = attendances,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Upcoming Gigs",
                SearchTerm = query

            };

            return View("Gigs", viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}