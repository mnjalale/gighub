﻿using GigHub.Core;
using GigHub.Core.ViewModels.Following;
using System.Web.Mvc;

namespace GigHub.Controllers
{
    public class FollowingController : BaseController
    {
        public FollowingController(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        [Authorize]
        public ActionResult ArtistsFollowing()
        {
            var followings = unitOfWork.FollowingRepository.GetArtistsUserIsFollowing(UserId);

            var followingViewModel = new FollowingsViewModel()
            {
                Artists = followings,
                ShowActions = User.Identity.IsAuthenticated,
                Heading = "Artists I'm Following"
            };

            return View(followingViewModel);
        }
    }
}