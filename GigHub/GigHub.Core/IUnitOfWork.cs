using GigHub.Core.Repositories;

namespace GigHub.Core
{
    public interface IUnitOfWork
    {
        IAttendanceRepository AttendanceRepository { get; }
        IFollowingRepository FollowingRepository { get; }
        IGenreRepository GenreRepository { get; }
        IGigRepository GigRepository { get; }
        INotificationRepository NotificationRepository { get; }
        IUserNotificationRepository UserNotificationRepository { get; }

        void Save();
    }
}