using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.ViewModels.Account
{
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}