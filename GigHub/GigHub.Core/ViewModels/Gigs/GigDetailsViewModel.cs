using GigHub.Core.Models;

namespace GigHub.Core.ViewModels.Gigs
{
    public class GigDetailsViewModel
    {
        public Gig Gig { get; set; }

        public bool IsFollowing { get; set; }
        public bool IsAttending { get; set; }
        public string Details => $"Performing at {Gig.Venue} on {Gig.DateTime.ToString("d MMM")} at {Gig.DateTime.ToString("HH:mm")}";
    }
}