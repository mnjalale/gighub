using GigHub.Core.Models;
using System.Collections.Generic;

namespace GigHub.Core.ViewModels.Following
{
    public class FollowingsViewModel
    {
        public IEnumerable<ApplicationUser> Artists { get; set; }
        public bool ShowActions { get; set; }
        public string Heading { get; set; }
    }
}