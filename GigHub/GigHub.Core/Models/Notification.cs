using GigHub.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GigHub.Core.Models
{
    public class Notification
    {
        protected Notification()
        {
            DateTime = DateTime.Now;
            UserNotifications = new Collection<UserNotification>();
        }

        private Notification(Gig gig, NotificationType type)
        {
            Gig = gig ?? throw new ArgumentNullException(nameof(gig));
            Type = type;
            DateTime = DateTime.Now;
            UserNotifications = new Collection<UserNotification>();
        }

        public int Id { get; private set; }
        public int GigId { get; private set; }
        public DateTime DateTime { get; private set; }
        public NotificationType Type { get; private set; }
        public DateTime? OriginalDateTime { get; private set; }
        public string OriginalVenue { get; private set; }

        //Navigation
        public Gig Gig { get; private set; }
        public ICollection<UserNotification> UserNotifications { get; set; }

        #region Factory Methods

        public static Notification GigCreated(Gig gig)
        {
            return new Notification(gig, NotificationType.GigCreated);
        }

        public static Notification GigUpdated(Gig newGig, DateTime originalDateTime, string originalVenue)
        {
            var notification = new Notification(newGig, NotificationType.GigUpdated);
            notification.OriginalDateTime = originalDateTime;
            notification.OriginalVenue = originalVenue;
            return notification;
        }

        public static Notification GigCancelled(Gig gig)
        {
            return new Notification(gig, NotificationType.GigCanceled);
        }

        #endregion
    }
}