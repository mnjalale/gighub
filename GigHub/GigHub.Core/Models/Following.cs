namespace GigHub.Core.Models
{
    public class Following
    {
        public string FollowerId { get; set; }
        public string FolloweeId { get; set; }

        // Navigation
        public ApplicationUser Follower { get; set; }
        public ApplicationUser Followee { get; set; }

    }
}