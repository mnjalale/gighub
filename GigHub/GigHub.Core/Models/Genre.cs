using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GigHub.Core.Models
{
    public class Genre
    {
        public Genre()
        {
            Gigs = new Collection<Gig>();
        }

        public byte Id { get; set; }

        public string Name { get; set; }

        public ICollection<Gig> Gigs { get; set; }
    }
}