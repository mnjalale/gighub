using System;

namespace GigHub.Core.Models
{
    public class UserNotification
    {
        protected UserNotification()
        {

        }

        public UserNotification(ApplicationUser user, Notification notification)
        {
            User = user ?? throw new ArgumentNullException(nameof(user));
            Notification = notification ?? throw new ArgumentNullException(nameof(notification));
        }

        public string UserId { get; private set; }
        public int NotificationId { get; private set; }
        public bool IsRead { get; private set; }

        // Navigation
        public ApplicationUser User { get; private set; }
        public Notification Notification { get; private set; }

        #region Methods

        public void Read()
        {
            IsRead = true;
        }

        #endregion

    }
}