using AutoMapper;
using GigHub.Core.Dtos;

namespace GigHub.Core.Models.Extensions
{
    public static class MappingExtensions
    {
        public static NotificationDto ToDto(this Notification notification)
        {
            return Mapper.Map<Notification, NotificationDto>(notification);
        }
    }
}