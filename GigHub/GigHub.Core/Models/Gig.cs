using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GigHub.Core.Models
{
    public class Gig
    {
        public Gig()
        {
            Attendances = new Collection<Attendance>();
            Notifications = new Collection<Notification>();
        }

        #region Properties

        public int Id { get; set; }
        public string ArtistId { get; set; }
        public byte GenreId { get; set; }
        public DateTime DateTime { get; set; }
        public string Venue { get; set; }
        public bool IsCancelled { get; private set; }

        //Navigation properties
        public ApplicationUser Artist { get; set; }
        public Genre Genre { get; set; }
        public ICollection<Attendance> Attendances { get; private set; }
        public ICollection<Notification> Notifications { get; set; }

        #endregion

        #region Methods

        public void Cancel()
        {
            // Cancel gig
            IsCancelled = true;

            // Create notification
            var notification = Notification.GigCancelled(this);

            // Send out a notification to all users attending the gig            
            var attendees = Attendances
                            .Select(c => c.Attendee)
                            .ToList();

            foreach (var attendee in attendees)
            {
                attendee.Notify(notification);
            }
        }

        public void Modify(DateTime dateTime, byte genreId, string venue)
        {
            // Create notification
            var notification = Notification.GigUpdated(this, DateTime, Venue);

            // Update gig
            DateTime = dateTime;
            GenreId = genreId;
            Venue = venue;

            // Notify attendees
            var attendees = Attendances
                            .Select(g => g.Attendee);

            foreach (var attendee in attendees)
            {
                attendee.Notify(notification);
            }
        }

        #endregion
    }
}