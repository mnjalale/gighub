using GigHub.Core.Models;
using System;
using System.Collections.Generic;

namespace GigHub.Core.Repositories
{
    public interface IGigRepository
    {
        void Add(Gig gig);
        Gig GetGig(int gigId);
        IEnumerable<Gig> GetGigsUserAttending(string userId);
        Gig GetGigWithAttendees(int gigId);
        IEnumerable<Gig> GetUpcomingGigs();
        IEnumerable<Gig> GetUpcomingGigsByArtist(string artistId);
        void Update(Gig gig, DateTime dateTime, byte genreId, string venue);
    }
}