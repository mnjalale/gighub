namespace GigHub.Core.Dtos
{
    public class CancelDto
    {
        public int GigId { get; set; }
    }
}