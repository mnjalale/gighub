using GigHub.Core.Models.Enums;
using System;

namespace GigHub.Core.Dtos
{
    public class NotificationDto
    {
        public DateTime DateTime { get; set; }

        public NotificationType Type { get; set; }

        public DateTime? OriginalDateTime { get; set; }

        public string OriginalVenue { get; set; }

        public GigDto Gig { get; set; }


        public string Message
        {
            get
            {
                string message;
                switch (Type)
                {
                    case NotificationType.GigCanceled:
                        message = $"{Gig.Artist.Name} has canceled the gig at {Gig.Venue} at {DateTime}";
                        break;
                    case NotificationType.GigUpdated:
                        message = "";
                        break;
                    case NotificationType.GigCreated:
                        message = "";
                        break;
                    default:
                        message = "";
                        break;
                }

                return message;

            }
        }
    }
}