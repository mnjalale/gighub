﻿using FluentAssertions;
using GigHub.Controllers.Api;
using GigHub.Core;
using GigHub.Core.Dtos;
using GigHub.Core.Models;
using GigHub.Core.Repositories;
using GigHub.Test.Controllers.Api.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Http.Results;

namespace GigHub.Test.Controllers.Api
{
    [TestClass]
    public class AttendancesControllerTests
    {
        private AttendancesController _controller;
        private Mock<IAttendanceRepository> _mockAttendanceRepository;
        private string _userId;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockAttendanceRepository = new Mock<IAttendanceRepository>();

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.SetupGet(c => c.AttendanceRepository).Returns(_mockAttendanceRepository.Object);

            _controller = new AttendancesController(mockUnitOfWork.Object);
            _userId = "1";
            _controller.MockCurrentUser(_userId, "bill@domain.com");

        }

        [TestMethod]
        public void Attend_AttendanceAlreadyExists_ShouldReturnBadRequest()
        {
            var attendanceDto = new AttendanceDto { GigId = 1 };

            var attendance = new Attendance()
            {
                AttendeeId = _controller.UserId,
                GigId = attendanceDto.GigId
            };

            _mockAttendanceRepository
                .Setup(c => c.GetAttendance(attendanceDto.GigId, _controller.UserId))
                .Returns(attendance);

            var result = _controller.Attend(attendanceDto);

            result.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [TestMethod]
        public void Attend_AttendanceDoesntExistExists_ShouldReturnOkResult()
        {
            var attendanceDto = new AttendanceDto { GigId = 1 };

            Attendance attendance = null;

            _mockAttendanceRepository
                .Setup(c => c.GetAttendance(attendanceDto.GigId, _controller.UserId))
                .Returns(attendance);

            var result = _controller.Attend(attendanceDto);

            result.Should().BeOfType<OkResult>();
        }


    }
}
