using FluentAssertions;
using GigHub.Core.Models;
using GigHub.Persistence;
using GigHub.Persistence.Repositories;
using GigHub.Test.Persistence.Repositories.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;


namespace GigHub.Test.Persistence.Repositories
{
    [TestClass]
    public class GigRepositoryTests
    {
        private GigRepository _repository;
        private Mock<DbSet<Gig>> _mockGigs;
        private Mock<IApplicationDbContext> _mockContext;
        private string _userId;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockGigs = new Mock<DbSet<Gig>>();
            _mockContext = new Mock<IApplicationDbContext>();
            _userId = "1";
            //_mockContext.Setup(c => c.Gigs).Returns(_mockGigs.Object);
            //_repository = new GigRepository(_mockContext.Object);
        }

        private void SetupRepository(IList<Gig> gigs)
        {
            _mockGigs.SetSource(gigs);
            _mockContext.Setup(c => c.Gigs).Returns(_mockGigs.Object);
            _repository = new GigRepository(_mockContext.Object);
        }

        [TestMethod]
        public void GetUpcomingGigsByArtist_GigIsInThePast_ShouldNotBeReturned()
        {
            var gig = new Gig() { DateTime = DateTime.Today.AddDays(-1), ArtistId = _userId };
            var source = new List<Gig> { gig };
            //_mockGigs.SetSource(source);
            SetupRepository(source);

            var gigs = _repository.GetUpcomingGigsByArtist("1");

            gigs.Should().BeEmpty();
        }

        [TestMethod]
        public void GetUpcomingGigsByArtist_GigIsCanceled_ShouldNotBeReturned()
        {
            var gig = new Gig() { DateTime = DateTime.Today.AddDays(1), ArtistId = _userId };
            gig.Cancel();

            var source = new List<Gig> { gig };
            SetupRepository(source);

            var gigs = _repository.GetUpcomingGigsByArtist("1");

            gigs.Should().BeEmpty();
        }

        [TestMethod]
        public void GetUpcomingGigsByArtist_GigIsForADifferentArtist_ShouldNotBeReturned()
        {
            var gig = new Gig() { DateTime = DateTime.Today.AddDays(1), ArtistId = _userId + "-" };

            var source = new List<Gig> { gig };
            SetupRepository(source);

            var gigs = _repository.GetUpcomingGigsByArtist(_userId);

            gigs.Should().BeEmpty();
        }

        [TestMethod]
        public void GetUpcomingGigsByArtist_GigIsForArtistAndIsNotCanceledAndIsInFuture_ShouldBeReturned()
        {
            var gig = new Gig() { DateTime = DateTime.Today.AddDays(1), ArtistId = _userId };

            var source = new List<Gig> { gig };
            SetupRepository(source);

            var gigs = _repository.GetUpcomingGigsByArtist(_userId);

            gigs.Should().Contain(gig);
        }

    }
}
