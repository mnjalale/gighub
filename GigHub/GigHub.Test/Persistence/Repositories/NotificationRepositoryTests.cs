﻿using GigHub.Core.Models;
using GigHub.Persistence;
using GigHub.Persistence.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Data.Entity;

namespace GigHub.Test.Persistence.Repositories
{
    [TestClass]
    public class NotificationRepositoryTests
    {

        [TestMethod]
        public void GetUnreadNotifications_NotificationIsNotForUser_ShouldReturnNothing()
        {
            var notifications = new Mock<DbSet<Notification>>();

            var mockContext = new Mock<IApplicationDbContext>();

            mockContext.Setup(c => c.Notifications).Returns(notifications.Object);

            var notificationRepository = new NotificationRepository(mockContext.Object);
        }
    }
}
