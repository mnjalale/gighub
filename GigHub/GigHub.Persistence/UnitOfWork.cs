﻿using GigHub.Core;
using GigHub.Core.Repositories;
using GigHub.Persistence.Repositories;

namespace GigHub.Persistence
{
    public class UnitOfWork : IUnitOfWork //: IDisposable
    {
        private readonly IApplicationDbContext _context;
        public UnitOfWork(IApplicationDbContext context)
        {
            _context = context;
        }

        private IAttendanceRepository _attendanceRepository;
        public IAttendanceRepository AttendanceRepository
        {
            get
            {
                if (_attendanceRepository == null)
                {
                    _attendanceRepository = new AttendanceRepository(_context);
                }

                return _attendanceRepository;
            }
        }

        private IFollowingRepository _followingRepository;
        public IFollowingRepository FollowingRepository
        {
            get
            {
                if (_followingRepository == null)
                {
                    _followingRepository = new FollowingRepository(_context);
                }

                return _followingRepository;
            }
        }

        private IGenreRepository _genreRepository;
        public IGenreRepository GenreRepository
        {
            get
            {
                if (_genreRepository == null)
                {
                    _genreRepository = new GenreRepository(_context);
                }

                return _genreRepository;
            }
        }

        private IGigRepository _gigRepository;
        public IGigRepository GigRepository
        {
            get
            {
                if (_gigRepository == null)
                {
                    _gigRepository = new GigRepository(_context);
                }

                return _gigRepository;
            }
        }

        private INotificationRepository _notificationRepository;
        public INotificationRepository NotificationRepository
        {
            get
            {
                if (_notificationRepository == null)
                {
                    _notificationRepository = new NotificationRepository(_context);
                }

                return _notificationRepository;
            }
        }

        private IUserNotificationRepository _userNotificationRepository;
        public IUserNotificationRepository UserNotificationRepository
        {
            get
            {
                if (_userNotificationRepository == null)
                {
                    _userNotificationRepository = new UserNotificationRepository(_context);
                }

                return _userNotificationRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        //public void Dispose()
        //{
        //    throw new NotImplementedException();
        //}
    }
}