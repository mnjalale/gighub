using GigHub.Core.Models;
using GigHub.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace GigHub.Persistence.Repositories
{
    public class GenreRepository : BaseRepository, IGenreRepository
    {
        public GenreRepository(IApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Genre> Get()
        {
            return context.Genres.ToList();
        }

    }
}