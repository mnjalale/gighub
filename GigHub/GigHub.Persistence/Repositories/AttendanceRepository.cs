using GigHub.Core.Models;
using GigHub.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GigHub.Persistence.Repositories
{
    public class AttendanceRepository : BaseRepository, IAttendanceRepository
    {
        public AttendanceRepository(IApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Attendance> GetFutureAttendances(string userId)
        {
            return context.Attendances
                            .Where(c => c.AttendeeId == userId && c.Gig.DateTime > DateTime.Now)
                            .ToList();
        }

        public Attendance GetAttendance(int gigId, string userId)
        {
            return context.Attendances.SingleOrDefault(c => c.GigId == gigId && c.AttendeeId == userId);
        }

        public void Add(Attendance attendance)
        {
            context.Attendances.Add(attendance);
        }

        public void Delete(Attendance attendance)
        {
            context.Attendances.Remove(attendance);
        }
    }
}