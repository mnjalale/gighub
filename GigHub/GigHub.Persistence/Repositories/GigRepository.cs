using GigHub.Core.Models;
using GigHub.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GigHub.Persistence.Repositories
{
    public class GigRepository : BaseRepository, IGigRepository
    {
        public GigRepository(IApplicationDbContext context) : base(context)
        {
        }

        public void Add(Gig gig)
        {
            context.Gigs.Add(gig);
        }

        public void Update(Gig gig, DateTime dateTime, byte genreId, string venue)
        {
            gig.Modify(dateTime, genreId, venue);

        }

        public Gig GetGig(int gigId)
        {
            return context.Gigs
                            .Include(c => c.Artist)
                            .Include(c => c.Genre)
                            .SingleOrDefault(c => c.Id == gigId);
        }

        public Gig GetGigWithAttendees(int gigId)
        {
            return context.Gigs
                        .Include(g => g.Attendances.Select(a => a.Attendee))
                        .SingleOrDefault(g => g.Id == gigId);
        }

        public IEnumerable<Gig> GetGigsUserAttending(string userId)
        {
            return context.Attendances
                             .Where(a => a.AttendeeId == userId)
                             .Select(a => a.Gig)
                             .Include(g => g.Artist)
                             .Include(g => g.Genre)
                             .ToList();
        }

        public IEnumerable<Gig> GetUpcomingGigs()
        {
            return context.Gigs
                    .Where(g => g.DateTime > DateTime.Now && !g.IsCancelled)
                    .Include(g => g.Genre);
        }

        public IEnumerable<Gig> GetUpcomingGigsByArtist(string artistId)
        {
            return context.Gigs
                        .Where(c => c.DateTime > DateTime.Now
                                && !c.IsCancelled
                                && c.ArtistId == artistId)
                        .Include(c => c.Genre)
                        .ToList();
        }

    }
}