﻿using GigHub.Core.Models;
using GigHub.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace GigHub.Persistence.Repositories
{
    public class UserNotificationRepository : BaseRepository, IUserNotificationRepository
    {
        public UserNotificationRepository(IApplicationDbContext context)
            : base(context)
        {
        }

        public IEnumerable<UserNotification> GetUnreadUserNotifications(string userId)
        {
            return context.UserNotifications.Where(c => c.UserId == userId && !c.IsRead).ToList();

        }
    }
}
