﻿using GigHub.Core.Models;
using GigHub.Core.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GigHub.Persistence.Repositories
{
    public class NotificationRepository : BaseRepository, INotificationRepository
    {
        public NotificationRepository(IApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Notification> GetUnreadNotifications(string userId)
        {
            return context.UserNotifications
                            .Where(c => c.UserId == userId && !c.IsRead)
                            .Select(c => c.Notification)
                            .Include(c => c.Gig.Artist)
                            .Include(c => c.Gig.Genre);
        }
    }
}
