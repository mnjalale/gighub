using GigHub.Core.Models;
using GigHub.Core.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GigHub.Persistence.Repositories
{
    public class FollowingRepository : BaseRepository, IFollowingRepository
    {
        public FollowingRepository(IApplicationDbContext context) : base(context)
        {
        }

        public Following GetFollowing(string artistId, string userId)
        {
            return context.Followings.SingleOrDefault(c => c.FolloweeId == artistId && c.FollowerId == userId);
        }

        public IEnumerable<ApplicationUser> GetArtistsUserIsFollowing(string userId)
        {
            return context.Followings
                            .Include(f => f.Followee)
                            .Where(f => f.FollowerId == userId)
                            .Select(f => f.Followee)
                            .ToList();
        }

        public void Add(Following following)
        {
            context.Followings.Add(following);
        }

        public void Delete(Following following)
        {
            context.Followings.Remove(following);
        }
    }
}