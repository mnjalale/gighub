namespace GigHub.Persistence.Repositories
{
    public abstract class BaseRepository
    {
        protected IApplicationDbContext context;

        public BaseRepository(IApplicationDbContext context)
        {
            this.context = context;
        }
    }
}