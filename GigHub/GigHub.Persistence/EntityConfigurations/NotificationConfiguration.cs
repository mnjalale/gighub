﻿using GigHub.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Persistence.EntityConfigurations
{
    public class NotificationConfiguration : EntityTypeConfiguration<Notification>
    {
        public NotificationConfiguration()
        {
            HasKey(c => c.Id);

            HasMany(c => c.UserNotifications)
                .WithRequired(c => c.Notification)
                .WillCascadeOnDelete();
        }
    }
}
