﻿using GigHub.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Persistence.EntityConfigurations
{
    public class GigConfiguration : EntityTypeConfiguration<Gig>
    {
        public GigConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.ArtistId)
                .IsRequired();

            Property(c => c.GenreId)
                .IsRequired();

            Property(c => c.Venue)
                .IsRequired()
                .HasMaxLength(255);

            HasMany(c => c.Attendances)
                .WithRequired(c => c.Gig)
                .WillCascadeOnDelete(false);

            HasMany(c => c.Notifications)
                .WithRequired(c => c.Gig)
                .WillCascadeOnDelete();
        }
    }
}