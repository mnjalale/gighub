﻿using GigHub.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Persistence.EntityConfigurations
{
    public class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(100);

            HasMany(c => c.Gigs)
                .WithRequired(c => c.Artist)
                .WillCascadeOnDelete();

            HasMany(c => c.Attendances)
                .WithRequired(c => c.Attendee)
                .WillCascadeOnDelete();

            HasMany(f => f.Followees)
                .WithRequired(f => f.Follower)
                .WillCascadeOnDelete(false);

            HasMany(f => f.Followers)
                .WithRequired(f => f.Followee)
                .WillCascadeOnDelete(false);

            HasMany(c => c.UserNotifications)
                .WithRequired(c => c.User)
                .WillCascadeOnDelete(false);
        }
    }
}