﻿using GigHub.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GigHub.Persistence.EntityConfigurations
{
    public class UserNotificationConfiguration: EntityTypeConfiguration<UserNotification>
    {
        public UserNotificationConfiguration()
        {
            HasKey(c => new { c.UserId, c.NotificationId });
        }
    }
}
